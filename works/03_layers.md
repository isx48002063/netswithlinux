#### 1. ip a

Borra todas las rutas y direcciones ip, para el servicio NetworkManager y asegúrate que no queda ningún demonio de dhclient corriendo. Comprueba que no queda ninguna con "ip a" y "ip r"
	ip r f all 
	ip a f dev enp2s0

	comprobem utilitzan el següent: 
	ip r
	ip a
	
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff


Ponte las siguientes ips en tu tarjeta ethernet: 
2.2.2.2/24, 
3.3.3.3/16, 
4.4.4.4/25

	$ ip a a 2.2.2.2/24	 dev enp2s0
	$ ip a a 3.3.3.3/16  dev enp2s0
	$ ip a a 4.4.4.4/25  dev enp2s0
	
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff
    inet 2.2.2.2/24 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 3.3.3.3/16 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 4.4.4.4/25 scope global enp2s0
       valid_lft forever preferred_lft forever

	

Consulta la tabla de rutas de tu equipo

Haz ping a las siguientes direcciones y justifica por qué en algunas sale el mensaje de "Network is unrecheable", en otras contesta y en otras se queda esperando sin dar mensajes de error:

2.2.2.2 , 2.2.2.254 , 2.2.5.2 , 3.3.3.35 , 3.3.200.45 , 4.4.4.8, 4.4.4.132

	Quan fem ping al 2.2.2.2/24 veiem que no dona cap mena de problema ja que /24 estem dins la mateixa xarxa

	En canvi els altres ens dona error, ja que o be no estem connectats a la mateixa red i no pot accedir o directament no estem al mateix rang

#### 2. ip lin


Borra todas las rutas y direcciones ip de la tarjeta ethernet

Conecta una segunda interfaz de red por el puerto usb

2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff
3: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:36:1c:2d brd ff:ff:ff:ff:ff:ff

Cambiale el nombre a usb0

	ip link set usb0 down

	ip link set usb0 name usbwlan



comprobem que el nom s'ha cambiat be
ip a
[...]
3: usbwlan: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:36:1c:2d brd ff:ff:ff:ff:ff:ff


Modifica la dirección MAC

# ip link set usbwlan address 00:11:22:33:44:55
# ip link set usbwlan up


3: usbwlan: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:11:22:33:44:55 brd ff:ff:ff:ff:ff:ff


Asígnale la direcció ip 5.5.5.5/24 a usb0 y 7.7.7.7/24 a la tarjeta de la placa base.
ip a a 5.5.5.5/24 dev usbwlan 
 ip a a 7.7.7.7/24 dev enp2s0


comprobem 

ip a



2: enp2s0: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff
    inet 7.7.7.7/24 scope global enp2s0
       valid_lft forever preferred_lft forever
3: usbwlan: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:11:22:33:44:55 brd ff:ff:ff:ff:ff:ff
    inet 5.5.5.5/24 scope global usbwlan
       valid_lft forever preferred_lft forever

Observa la tabla de rutas
5.5.5.0/24 dev usbwlan  proto kernel  scope link  src 5.5.5.5 linkdown 

#### 3. iperf

Borra todas las rutas y direcciones ip de la tarjeta ethernet

En cada ordenador os ponéis la ip 172.16.99.XX/24 (XX=puesto de trabajo)

ip a a 172.16.99.11/24 dev enp2s0

2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 94:de:80:49:7d:9f brd ff:ff:ff:ff:ff:ff
    inet 172.16.99.11/24 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet6 fe80::96de:80ff:fe49:7d9f/64 scope link 
       valid_lft forever preferred_lft forever

Lanzar iperf en modo servidor en cada ordenador
iperf -s  ( es la orden para hacer de servidor)
iperf -c  172.16.99.11 (es para el cliente )

Comprueba con netstat en qué puerto escucha

 netstat -tlnp 


    tshark -i enp2s0 -w /var/tmp/captura.pcap -c30 

    tshark -i enp2s0 -w /var/tmp/captura.pcap -c30 



Encontrar los 3 paquetes del handshake de tcp



Abrir dos servidores en dos puertos distintos

Observar como quedan esos puertos abiertos con netstat

Conectarse al servidor con dos clientes y que la prueba dure 1 minuto

Mientras tanto con netstat mirar conexiones abiertas


[isx48002063@j11 ~]$ tshark -r isx48002063/David/Xarxes/out.pcap -x -Y "frame.number==2"
0000  14 da e9 b6 e8 ea 14 da e9 99 a1 bc 08 00 45 00   ..............E.
0010  00 3c 04 81 40 00 40 06 18 05 ac 10 63 0b ac 10   .<..@.@.....c...
0020  63 0a b2 e4 13 89 5b ab b7 39 00 00 00 00 a0 02   c.....[..9......
0030  72 10 29 db 00 00 02 04 05 b4 04 02 08 0a 00 d3   r.).............
0040  b3 b8 00 00 00 00 01 03 03 07                     ..........



[isx48002063@j11 ~]$ tshark -r isx48002063/David/Xarxes/out.pcap -x -Y "frame.number==3"
0000  14 da e9 99 a1 bc 14 da e9 b6 e8 ea 08 00 45 00   ..............E.
0010  00 3c 00 00 40 00 40 06 1c 86 ac 10 63 0a ac 10   .<..@.@.....c...
0020  63 0b 13 89 b2 e4 be 68 7d 63 5b ab b7 3a a0 12   c......h}c[..:..
0030  71 20 fd 18 00 00 02 04 05 b4 04 02 08 0a 00 78   q .............x
0040  f1 5c 00 d3 b3 b8 01 03 03 07  


[isx48002063@j11 ~]$ tshark -r isx48002063/David/Xarxes/out.pcap -x -Y "frame.number==4"
0000  14 da e9 b6 e8 ea 14 da e9 99 a1 bc 08 00 45 00   ..............E.
0010  00 34 04 82 40 00 40 06 18 0c ac 10 63 0b ac 10   .4..@.@.....c...
0020  63 0a b2 e4 13 89 5b ab b7 3a be 68 7d 64 80 10   c.....[..:.h}d..
0030  00 e5 9c 20 00 00 01 01 08 0a 00 d3 b3 b8 00 78   ... ...........x
0040  f1 5c                                             .\



Abrir dos servidores en dos puertos distintos

	iperf -s -i 3 -p 5004

	iperf -s i 4 -p 5005



Observar como quedan esos puertos abiertos con netstat

	[isx48002063@j11 ~]$ netstat -utlnp
	(Not all processes could be identified, non-owned process info
	will not be shown, you would have to be root to see it all.)
	Active Internet connections (only servers)
	Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
	tcp        0      0 0.0.0.0:5004            0.0.0.0:*               LISTEN      -                   
	tcp        0      0 0.0.0.0:5005            0.0.0.0:*               LISTEN 


Conectarse al servidor con dos clientes y que la prueba dure 1 minuto

	[isx48002063@j11 ~]$ iperf -t 60 -c 172.16.99.12 
	------------------------------------------------------------
	Client connecting to 172.16.99.12, TCP port 5001
	TCP window size: 85.0 KByte (default)
	------------------------------------------------------------
	[  3] local 172.16.99.11 port 47172 connected with 172.16.99.12 port 5001
	[ ID] Interval       Transfer     Bandwidth
	[  3]  0.0-60.0 sec   674 MBytes  94.2 Mbits/sec


Mientras tanto con netstat mirar conexiones abiertas

	netstat -utlnp

