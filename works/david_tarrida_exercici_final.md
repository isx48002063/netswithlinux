Una organización está organizada en dos departamentos:
- deptA
- deptB

Cada departamento ha de estar aislado del otro y ha de poder acceder
a una red de servidores que llamaremos "servers". También se dipone de 
una red wifi que no ha de poder acceder a las redes internas, y que sólo debe dejar
acceder al puerto 80 y 443 del servidor web de la red de servers. Desde
la red wifi también se podrá navegar a internet usando los puertos destino 80 y 443, pero
no se podrán establecer conexiones telnet, ssh o ftp.

Se ha contratado una línea empresarial a un ISP, que ha de ser utilizada
sólo por los servidores, excepto en caso de caída del "router inf" que
es el que se usa habitulamente como router de salida de las redes de 
los departamentos y de los dispositivos conectados por wifi.

Se ha de priorizar el tráfico del deptA respecto al deptB y se ha de 
repartir el ancho de banda por igual entre los usuarios de cada departamento.

Se ha de limitar el tráfico de bajada de la wifi a 500Kbps

El servidor ha de ser accesible desde inernet usando el puerto externo 77XX

Se ha de limitar el acceso por ssh (cambiando el puerto al 12322) al 
router desde cualquier red, y sólo se ha de poder hacer telnet desde la red de control

Se disone de un mikrotik de 4 puertos y de 6 puertos del switch para cada
puesto de trabajo que se conectarán de la siguiente forma:


MIKROTIK                   tagged               untagged   destino 
--------------------------------------------------------------------------
puerto 1   internet        escola,isp                      sw-puerto4 
puerto 2   puerto usb pc                                   pc-usb-ethernet 
puerto 3   redes internas  deptA,deptB,servers             sw-puerto5 
puerto 4      

     
SWITCH                   tagged          untagged type destino
-------------------------------------------------------------------------------------------
puerto 1   deptA                                         deptA      access    PC
puerto 2   detpB                                         deptB      access    PC
puerto 3   servers                                       servers    access    PC
puerto 4   mkt-internet         escola,isp                          trunk     mkt-puerto1
puerto 5   mkt-xarxes_internes  deptA,deptB,servers                 trunk     mkt-puerto3
puerto 6   roseta               deptA,deptB,servers,isp  escola     hybrid    roseta


Tabla de redes, ips y vlans:

REDES      VLAN      NETWORK-ADDR    IP-ADDR-MKT   DHCP-RANGE
---------------------------------------------------------------
deptA      7xx      10.27.1XX.0/24   .1            .100 - .200
deptB      8xx      10.28.1XX.0/24   .1            .100 - .200
servers    9xx      10.29.1XX.0/24   .1            NO DHCP
escola     1000     192.168.0.0/16   .3.2XX        NO DHCP
isp        1005       10.30.0.0/24   .2XX          NO DHCP
wireless   NO-VLAN  10.50.1XX.0/24   .1            .10 - .250
gestion    NO-VLAN 192.168.88.0/24   .1            NO DHCP


El esquema a nivel de capa 3 es el siguiente.


                    +-----------+             XX XXXXX XXXX
                    |router inf +----------+XXXXXX         XX
                    +-----------+        XX                 X
                          |.1            XX   internet      X  +-------------+
                          |               X              XXXX  |  router ISP |
             -------------+----+-+        XXXXXXXXXXXXXXX+-----+             |
               192.168.0.0/16  |                               +-------------+
                               |                                    |.1
                               |             10.30.0.0/24           |
                               |    +-+-----------------------------++
  +------+                     |      |
  |      |              escola |      | isp
  |server|            vlan1000 |      | vlan1005
  |web   |               port1 |      | port1
  +------+               .3.2XX|      |.2xx
     |.100                  +-------------+
     |                      |             |       +))
     |    10.29.1xx.0/24  .1|             |.1     |wireless
+----+----------------------+   MIKROTIK  +-------+
            vlan9xx    port3|             |    10.50.1xx.0/24
            servers         |             |
                            +--+-----+----+
                            .1 |     |  .1
                       vlan7xx |     |  vlan8xx
                         port3 |     |  port3
                         deptA |     |  deptB
                               |     |
                               |     |
      +------------------------+-+   |
          10.27.1xx.0/24           +-+------------------------------+
                                         10.28.1xx.0/24


# Resolución

Pasos:
1. Configurar switch
2. Configurar interfaces virtuales y direcciones IP en el router

[admin@mkt11] > ip address print                                 
Flags: X - disabled, I - invalid, D - dynamic 
 #   ADDRESS            NETWORK         INTERFACE                                                                                          
 0   ;;; defconf
     192.168.88.1/24    192.168.88.0    eth2                                                                                               
 1   10.27.111.1/24     10.27.111.0     deptA                                                                                              
 2   10.28.111.1/24     10.28.111.0     deptB                                                                                              
 3   10.30.0.211/24     10.30.0.0       isp                                                                                                
 4   192.168.3.211/16   192.168.0.0     escola                                                                                             
 5   10.29.111.100/24   10.29.111.0     servers                                                                                            
 6   10.50.111.1/24     10.50.111.0     wifi-david                                                                                         

3. Configurar servidores dhcp
4. Definir default gateway por router inf y verificar que salen, cambiar
default gateway por router-isp y verificar que salen. Dejar la ruta con más 
peso a router-inf. Verificar que al desconectar router-inf se cambia la salida por
router-isp
5. Activar las marcas de rutas para que desde la vlan de servers se salga
por router-isp y desde el resto de redes se salga por router-inf

[admin@mkt11] > ip firewall mangle add action=mark-connection chain=prerouting connection-state=new new-connection-mark=from_servers src-address=10.29.111.0/24
[admin@mkt11] > ip firewall mangle add action=mark-routing chain=prerouting 
[admin@mkt11] > ip firewall mangle add action=mark-routing chain=prerouting in-interface=servers new-routing-mark=servers_to_isp
[admin@mkt11] > ip route add dst-address=0.0.0.0/0 gateway=10.30.0.1 routing-mark=from_servers 



[admin@mkt11] > ip route print 
Flags: X - disabled, A - active, D - dynamic, C - connect, S - static, r - rip, b - bgp, o - ospf, m - mme, 
B - blackhole, U - unreachable, P - prohibit 
 #      DST-ADDRESS        PREF-SRC        GATEWAY            DISTANCE
 0 A S  0.0.0.0/0                          10.30.0.1                 1
 1 A S  0.0.0.0/0                          192.168.0.1               1
 2   S  0.0.0.0/0                          10.30.0.1                 2
 3 ADC  10.27.111.0/24     10.27.111.1     deptA                     0
 4 ADC  10.28.111.0/24     10.28.111.1     deptB                     0
 5 ADC  10.29.111.0/24     10.29.111.100   servers                   0
 6 ADC  10.30.0.0/24       10.30.0.211     isp                       0
 7  DC  10.50.111.0/24     10.50.111.1     wifi-david              255
 8 ADC  192.168.0.0/16     192.168.3.211   escola                    0
 9 ADC  192.168.88.0/24    192.168.88.1    eth2                      0



6. Reglas de firewall para evitar accesos desde deptA a deptB y desde
wifi solo a server-web y a internet


[admin@mkt11] > ip firewall filter add chain=forward src-address=10.27.111.0/24 dst-address=10.28.111.0/24 action=drop 
[admin@mkt11] > ip firewall filter add chain=forward src-address=10.28.111.0/24 dst-address=10.27.111.0/24 action=drop   
[admin@mkt11] > ip firewall filter add chain=forward connection-state=established,related action=accept place-before=0

 9    chain=forward action=drop src-address=10.27.111.0/24 dst-address=10.28.111.0/24 log=no log-prefix="" 

10    chain=forward action=drop src-address=10.28.111.0/24 dst-address=10.27.111.0/24 log=no log-prefix="" 

11    chain=forward action=accept dst-address=10.29.111.100 in-interface=wifi-david log=no log-prefix="" 

12    chain=forward action=accept in-interface=wifi-david out-interface=escola log=no log-prefix="" 

13    chain=forward action=accept in-interface=wifi-david out-interface=isp log=no log-prefix="" 

14    chain=forward action=drop in-interface=wifi-david log=no log-prefix="" 


7. Reglas de firewall para que sólo haya telnet desde ips de gestión 


[admin@mkt11] > ip firewall filter add chain=input dst-port=23 protocol=tcp src-address=192.168.88.0/24 action=accept 
[admin@mkt11] > ip firewall filter add chain=input dst-port=23 protocol=tcp  action=drop 


y servidor ssh por puerto 12322 

[admin@mkt11] > ip service set  ssh port=12322


8. Limitar ancho de banda de bajada en wireless a 500Kbps

Flags: X - disabled, I - invalid 
 0   name="wifi_que" parent=wifi-david packet-mark=no-mark limit-at=500k queue=default priority=8 max-limit=500k burst-limit=0 
     burst-threshold=0 burst-time=0s 
[admin@mkt11] > 

9. Priorizar tráfico de descarga del deptA respecto a deptB
10. Repartir por igual el tráfico entre todos los usuarios de deptA y deptB


/queue tree
add name=deptB packet-mark=no-mark parent=deptB queue=\
    pcq-download-default
add name=deptA packet-mark=no-mark parent=deptA queue=\
    pcq-download-default


##################EXPORT!!!############################################



[admin@mkt11] > /export                                                                                                            
# may/24/2017 13:13:38 by RouterOS 6.33.5
# software id = KHNI-GJGJ
#
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce disabled=no distance=indoors frequency=auto mode=ap-bridge \
    name=wifi-david ssid="Mikrotik David" wireless-protocol=802.11
/interface ethernet
set [ find default-name=ether1 ] name=eth1
set [ find default-name=ether2 ] name=eth2
set [ find default-name=ether3 ] name=eth3
set [ find default-name=ether4 ] name=eth4
/ip neighbor discovery
set eth1 discover=no
/interface vlan
add interface=eth3 name=deptA vlan-id=711
add interface=eth3 name=deptB vlan-id=811
add interface=eth1 name=escola vlan-id=1000
add interface=eth1 name=isp vlan-id=1005
add interface=eth3 name=servers vlan-id=911
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
/ip pool
add name=rangedeptA ranges=10.27.111.100-10.27.111.200
add name=rangedeptB ranges=10.28.111.100-10.28.111.200
add name=range-wifi ranges=10.50.111.10-10.50.111.250
add name=rangewifi ranges=10.50.111.10-10.50.111.250
/ip dhcp-server
add address-pool=rangedeptA disabled=no interface=deptA name=dhcp1
add address-pool=rangedeptB disabled=no interface=deptB name=dhcp2
add address-pool=rangewifi disabled=no interface=wifi-david name=dhcp3
/queue tree
add limit-at=500k max-limit=500k name=wifi_que packet-mark=no-mark parent=wifi-david queue=default
/ip address
add address=192.168.88.1/24 comment=defconf interface=eth2 network=192.168.88.0
add address=10.27.111.1/24 interface=deptA network=10.27.111.0
add address=10.28.111.1/24 interface=deptB network=10.28.111.0
add address=10.30.0.211/24 interface=escola network=10.30.0.0
add address=192.168.3.211/16 interface=escola network=192.168.0.0
add address=10.29.111.1/24 interface=servers network=10.29.111.0
add address=10.50.111.1/24 interface=wifi-david network=10.50.111.0
/ip dhcp-server network
add address=10.27.111.0/24 dns-server=8.8.8.8 domain=departamentA.com gateway=10.27.111.1 netmask=24
add address=10.28.111.0/24 dns-server=8.8.8.8 domain=departamentB.com gateway=10.28.111.1 netmask=24
add address=10.50.111.0/24 dns-server=8.8.8.8 domain=wifi.com gateway=10.50.111.1 netmask=24
/ip firewall filter
add chain=forward connection-state=established,related
add chain=input comment="defconf: accept ICMP" protocol=icmp
add chain=input comment="defconf: accept establieshed,related" connection-state=established,related
add action=drop chain=input comment="defconf: drop all from WAN" in-interface=eth1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" connection-state=established,related
add chain=forward comment="defconf: accept established,related" connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" connection-state=invalid
add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" connection-nat-state=!dstnat connection-state=new \
    in-interface=eth1
add action=drop chain=forward dst-address=10.28.111.0/24 src-address=10.27.111.0/24
add action=drop chain=forward dst-address=10.27.111.0/24 src-address=10.28.111.0/24
add chain=forward dst-address=10.29.111.100 in-interface=wifi-david
add chain=forward in-interface=wifi-david out-interface=escola
add chain=forward in-interface=wifi-david out-interface=isp
add action=drop chain=forward in-interface=wifi-david
add chain=input dst-port=23 protocol=tcp src-address=192.168.88.0/24
add action=drop chain=input dst-port=23 protocol=tcp
/ip firewall mangle
add action=mark-connection chain=prerouting connection-state=new new-connection-mark=from_servers src-address=10.29.111.0/24
add action=mark-routing chain=prerouting in-interface=servers new-routing-mark=servers_to_isp
/ip firewall nat
add action=masquerade chain=srcnat out-interface=escola
/ip route
add distance=1 gateway=10.30.0.1 routing-mark=from_servers
add distance=1 gateway=10.30.0.1 routing-mark=from_servers
add distance=1 gateway=192.168.0.1
add distance=2 gateway=10.30.0.1
/ip service
set ssh port=12322
/system clock
set time-zone-name=Europe/Madrid
/system identity
set name=mkt11
/system routerboard settings
set cpu-frequency=650MHz protected-routerboot=disabled
/tool mac-server
set [ find default=yes ] disabled=yes
add interface=deptA
/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes
add interface=deptA






