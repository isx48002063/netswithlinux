!#/bin/bash
systemctl stop NetworkManager

name_from_fedora_A=enp0s18f1u2c2
name_from_fedora_B=usb0
name_from_fedora_C=enp0s18f1u3

label_A=12
label_B=11
label_C=10

ip link set $name_from_fedora_A down
ip link set $name_from_fedora_B down
ip link set $name_from_fedora_C down

ip link set $name_from_fedora_A name usbA
ip link set $name_from_fedora_B name usbB
ip link set $name_from_fedora_C name usbC

ip link set usbA address aa:aa:aa:00:00:$label_A
ip link set usbB address aa:aa:aa:00:00:$label_B
ip link set usbC address aa:aa:aa:00:00:$label_C

ip link set usbA up
ip link set usbB up
ip link set usbC up


ip a a 172.17.12.1/24 dev usbA
ip a a 172.17.10.1/24 dev usbC

dhclient -r
dhclient usbB 
