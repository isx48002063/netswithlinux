BRIDGES Y ROUTERS

##### 1. Conexionado, nombres de interfaces y direcciones MAC

Para esta práctica hay que interconectar los equipos de una fila con 2 latiguillos de red (sin usar las rosetas del aula) siguiendo el siguiente esquema:

```
FILA DE 3 EQUIPOS:
                              |usbB                          
   +--------+            +--------+            +--------+   
   |        |eth0    usbA|        |usbC    eth0|        |   
   | HOST A +------------+ HOST B +------------+ HOST C |   
   |        |            |        |            |        |   
   +--------+            +--------+            +--------+   
                             |eth0                          
```

El equipo del medio de la fila ha de tener conectadas 3 tarjetas usb-ethernet. Se han de renombrar estas tarjetas y cambiar las mac. Las direcciones mac de cada tarjeta han de ser distintas siguiendo la siguiente
codificación:

AA:AA:AA:00:00:YY

Siendo YY la numeración de la tarjeta USB que lleva en su pegatina.


Hay que renombrar las tarjetas de red para que se llamen: usbA, usbB, usbC y eth0(la de la placa base)

Al final la orden ip link show ha de mostrar algo como lo siguiente:


# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 14:da:e9:99:a1:bc brd ff:ff:ff:ff:ff:ff
    inet 192.168.3.11/16 brd 192.168.255.255 scope global dynamic eth0
       valid_lft 18967sec preferred_lft 18967sec
4: usbC: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether aa:aa:aa:00:00:10 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::a8aa:aaff:fe00:10/64 scope link 
       valid_lft forever preferred_lft forever
5: usbA: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN group default qlen 1000
    link/ether aa:aa:aa:00:00:12 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::a8aa:aaff:fe00:12/64 scope link 
       valid_lft forever preferred_lft forever
6: usbB: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether aa:aa:aa:00:00:11 brd ff:ff:ff:ff:ff:ff



##### 2. Conectar 3 equipos haciendo routing y saliendo a internet:

A. Configurar las ips en cada equpo según el esquema y hacer pings desde hostA a hostB y desde hostC a hostA

```
ESQUEMA CAPA 3
                       +---------+
    172.17.A.0/24    .1| ROUTER  |.1   172.17.C.0/24
   +-------------------+ HOST B  +--------------------+
        |.2        usbA|         |usbC        |.2
        |eth0          +---------+            |eth0
     +------+                              +------+
     |HOST A|                              |HOST C|
     +------+                              +------+
                            
          
  571  ip a a 172.17.11.1/24 dev usbB
  572  ip a a 172.17.12.1/24 dev usbA
  573  ip a a 172.17.10.1/24 dev usbC
                       

```

B. Activar el bit de forwarding y poner como default gateway al host B que hará de router entre las dos redes, listar las rutas en host B y verificar que se pueden hacer pings entre HOST A y HOST C

	echo 1 > /proc/sys/net/ipv4/ip_forward


C. Conectar eth0 del HOST B a la roseta del aula, pedir una ip dinámica con dhclient. Verificar que desde el HOST B se puede hacer ping al 8.8.8.8. 

[root@j11 ~]# ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=3 ttl=55 time=12.3 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=55 time=10.7 ms
64 bytes from 8.8.8.8: icmp_seq=5 ttl=55 time=10.5 ms


D. Activar el enmascarmiento de ips para el tráfico saliente por eth0 y verificar que HOST A Y HOST C pueden hacer ping al 8.8.8.8

#iptables -t nat -A POSTROUTING -o enp2s0  -j MASQUERADE  'host B'


como host A y C 

hacemos un flux de las rutas de sortida en los dos host ' A y C'

ip r f all ' host A y C'


# ip r a default via 172.17.12.1 'host A'

# ip  r a default via 172.17.10.1 'host C'


#ping 8.8.8.8  ' host A y C'


responde correctamente !!!






E. Poner como servidor dns a 8.8.8.8 en /etc/resolv.conf en hostA y host C y verificar que se puede navegar por internet con  haciendo un wget de http://www.netfilter.org

host A y C

#echo "nameserver 8.8.8.8" > /etc/resolv.conf


y comprovamos que tenemos internet 

# wget http://netfilter.org



##### 3. Conectar 2 equipos utilizando un equipo intermedio que hace de bridge. 

En esta práctica queremos que HOST B trabaje como si fuera un switch, interconectando a host A y host C. 

A. Hacer flush de todas las ips y verificar que no queda ninguna ruta ni dirección ip asociada a ningún equipo.

B. Crear el bridge br0 y añadir usbA y usbC a ese bridge. Listar con detalle la configuración del bridge.

C. En el host A nos ponemos la ip 192.168.100.A/24 y en el equipo C la ip 192.168.100.B/24. Hacemos ping entre los dos equipos y debería de ir.

D. Listar en host B la tabla de relación de puertos y MACs en el bridge


##### 4. Conectar todos los equipos del aula haciendo switching.

Conseguir esta interconexión entre las filas:
```
\
                   |                     
                   +------+              
                          |              
  BRIDGE : br0            |              
 +-------------------------------------+ 
 | +---+  +---+  +---+  +---+          | 
 | | 1 |  | 2 |  | 3 |  | 4 |   HOST B | 
 | +---+  +---+  +---+  +---+          | 
 |   |usbA  |usbC  |eth0   usbB        | 
 +-------------------------------------+ 
     |      |      |                     
     |      |      |                     
     |      |      |                     
 +---+--+ +-+----+ |                     
 |HOST A| |HOST C| +------+              
 +------+ +------+        |              
                          |              
                          |              
  BRIDGE : br0            |              
 +-------------------------------------+ 
 | +---+  +---+  +---+  +---+          | 
 | | 1 |  | 2 |  | 3 |  | 4 |   HOST B | 
 | +---+  +---+  +---+  +---+          | 
 |   |usbA  |usbC  |eth0   usbB        | 
 +-------------------------------------+ 
     |      |      |                     
     |      |      |                     
     |      |      |                     
 +---+--+ +-+----+ |                     
 |HOST A| |HOST C| +------+
 +------+ +------+        |
                          |
                          +
```
Cada equipo ha de tener una ip 192.168.100.B/24

A. Lanzar fpings para verificar que todos los equipos A y C responden

B. Asignar una ip al br0 del hostB y lanzar fping para verificar que todos los equipos A,B y C responden

C. Listar la tabla de asignación de puertos y MACs después de hacer un fping 

##### 5. Conectar todos los equipos del aula haciendo routing.

```
\
                         +
                         |
                         |
                         |
                     +---+-----+
  172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24
 +-------------------+ HOST B  +--------------------+
      |.2            |         |          .2|
      |              +---------+            |
   +--+---+              |.1             +--+---+
   |HOST A|              |               |HOST C|
   +------+              |               +------+
                         |
                         |
                         |172.16.XX.0/24
                         |
                         |
                         |
                     usbB|.2
                     +---------+
  172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24
 +-------------------+ HOST B  +--------------------+
      |.2        usbA|         |usbC        |.2
      |eth0          +---------+            |eth0
   +------+          eth0|.1             +------+
   |HOST A|              |               |HOST C|
   +------+              |               +------+
                         |
                         |
                         |
                         +
```

A. Introducir todas las rutas necesarias en el host B y listarlas

B. Lanzar fping para verificar que todos los equipos A, B y C responden

##### 6. Conectar todos los equipos del aula haciendo switching y routing.

Seguir este esquema


```
                                       172.16.0.0/24
           +------------------------------------------------------------------------------+
                        |.XX                                                     |.XX
                        |                                                        |
                        |br0(usbB,eth0)                                          |br0(usbB,eth0)
                    +---------+                                              +---------+
 172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24        172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24
+-------------------+ HOST B  +--------------------+     +-------------------+ HOST B  +--------------------+
     |.2        usbA|         |usbC        |.2                |.2        usbA|         |usbC        |.2
     |eth0          +---------+            |eth0              |eth0          +---------+            |eth0
  +------+                              +------+           +------+                              +------+
  |HOST A|                              |HOST C|           |HOST A|                              |HOST C|
  +------+                              +------+           +------+                              +------+

```

A. Introducir todas las rutas necesarias en el host B y listarlas

B. Lanzar fping para verificar que todos los equipos A, B y C responden
