EJERCICIO MIKROTIK 2.

OBJETIVO: CREAR DOS REDES WIFIS CON DISTINTOS TIPOS DE SEGURIDAD


 MIKROTIK
 +-------------------+
 |  1  2  3  4  wifi |
 +-------------------+
    |  |  |  |
    |  |  |  |
    |  |  |  +
    |  |  |  Conexión a un switch  con vlans
    |  |  +
    |  |  Interface de red placa base
    |  +
    |  Interface USB
    |  Ip 192.168.88.111/24
    +
    Punto de red del aula
    para salida a internet

## Pasos para conseguir tener 2 redes wifis separadas con distintos
niveles de seguridad.

Recordamos que:
- Solo disponemos de una interface wifi que trabaja en la banda de 2,4GHz.
- Este modelo de mikrotik dispone de 4 interfaces ethernet y 1 interface wifi
- En el PC dispondremos de una interface USB que es la que usaremos para
configurar el router por el puerto 2

Para conseguir dos redes wifi a partir de una sola interface hay que crear
sub-interfaces o interfaces virtuales. Cada interface virtual ha de estar 
asociada a un id de vlan (número de 1 a 2048)

Si queremos comunicarnos en la misma red entre un dispositivo wifi y un dispositivo
cableado hay que crear bridges entre la interface wifi y la interface ethernet.

### Eliminar configuración inicial
La configuración inicial que viene con la mikrotik podemos consultarla haciendo 
/export y obtenmos:

```
/interface bridge
add admin-mac=6C:3B:6B:30:B8:4F auto-mac=no comment=defconf name=bridge
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce \
    disabled=no distance=indoors frequency=auto mode=ap-bridge ssid=\
    MikroTik-30B852 wireless-protocol=802.11
/interface ethernet
set [ find default-name=ether2 ] name=ether2-master
set [ find default-name=ether3 ] master-port=ether2-master
set [ find default-name=ether4 ] master-port=ether2-master
/ip neighbor discovery
set ether1 discover=no
set bridge comment=defconf
/ip pool
add name=default-dhcp ranges=192.168.88.10-192.168.88.254
/ip dhcp-server
add address-pool=default-dhcp disabled=no interface=bridge name=defconf
/interface bridge port
add bridge=bridge comment=defconf interface=ether2-master
add bridge=bridge comment=defconf interface=wlan1
/ip address
add address=192.168.88.1/24 comment=defconf interface=bridge network=\
    192.168.88.0
/ip dhcp-client
add comment=defconf dhcp-options=hostname,clientid disabled=no interface=ether1
/ip dhcp-server network
add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
/ip dns
set allow-remote-requests=yes
/ip dns static
add address=192.168.88.1 name=router
/ip firewall filter
add chain=input comment="defconf: accept ICMP" protocol=icmp
add chain=input comment="defconf: accept established,related" connection-state=\
    established,related
add action=drop chain=input comment="defconf: drop all from WAN" in-interface=\
    ether1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" \
    connection-state=established,related
add chain=forward comment="defconf: accept established,related" \
    connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" connection-state=\
    invalid
add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" \
    connection-nat-state=!dstnat connection-state=new in-interface=ether1
/system routerboard settings
set boot-device=flash-boot cpu-frequency=650MHz protected-routerboot=disabled
/tool mac-server
set [ find default=yes ] disabled=yes
add interface=bridge
/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes

```

A continuación modificamos todas esas partes de la configuración que no nos
interesan para dejar la mikrotik pelada, sin configuraciones iniciales
que interfieran con nuestra nueva programación del router:

```
# Cambiamos dirección IP para que la gestione directamente la interface 2
ip address set 0 interface=ether2-master

# Eliminamos el bridge

/interface bridge port remove 1
/interface bridge port remove 0
/interface bridge remove 0  

# Eliminamos que un puerto sea master de otro

/interface ethernet set [ find default-name=ether3 ] master-port=none
/interface ethernet set [ find default-name=ether4 ] master-port=none

# Cambiamos nombres a los puertos

/interface ethernet set [ find default-name=ether1 ] name=eth1
/interface ethernet set [ find default-name=ether2 ] name=eth2
/interface ethernet set [ find default-name=ether3 ] name=eth3
/interface ethernet set [ find default-name=ether4 ] name=eth4

# Deshabilitamos la wifi

/interface wireless set [ find default-name=wlan1 ] disabled=yes

# Eliminamos servidor y cliente dhcp

/ip pool remove 0
/ip dhcp-server network remove 0
/ip dhcp-server remove 0
/ip dhcp-client remove 0
/ip dns static remove 0
/ip dns set allow-remote-requests=no

# Eliminamos regla de nat masquerade
/ip firewall nat remove [/ip firewall nat find]

# Opcionalmente eliminamos todas las reglas de filter y mangle
# pero hay que ser conscientes que dejaremos el router accesible
# por telnet desde cualqauier puerto
/ip firewall filter remove [/ip firewall filter find]
/ip firewall mangle remove [/ip firewall mangle find]

```

Ahora le cambiamos el nombre y guardamos la configuración inicial por
si la necesitamos restaurar en cualquier momento
```
/system identity set name=mkt11
/system backup save name="20170317_zeroconf"
```

###[ejercicio1] Explica el procedimiento para resetear el router y restaurar
este backup, verifica con /export que no queda ninguna configuración inicial 
que pueda molestarnos para empezar a programar el router

Reseteamos la configuracion i cargamos el backup que tenemos guardado previamente,
los ficheros que tenemos guardados en el mikortik se encuentran en /file print.

		[admin@MikroTik] > system reset-configuration
		Dangerous! Reset anyway? [y/N]: y
		
		/system reset-configuration
		/system backup load name=20170317_zeroconf.backup 
```
[admin@mkt11] > /export 
# jan/02/1970 00:00:37 by RouterOS 6.33.5
# software id = KHNI-GJGJ
#
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce \
    distance=indoors frequency=auto mode=ap-bridge ssid=MikroTik-C38993 \
    wireless-protocol=802.11
/interface ethernet
set [ find default-name=ether1 ] name=eth1
set [ find default-name=ether2 ] name=eth2
set [ find default-name=ether3 ] name=eth3
set [ find default-name=ether4 ] name=eth4
/ip neighbor discovery
set eth1 discover=no
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
/ip address
add address=192.168.88.1/24 comment=defconf interface=eth2 network=\
    192.168.88.0
/ip firewall filter
add chain=input comment="defconf: accept ICMP" protocol=icmp
add chain=input comment="defconf: accept establieshed,related" \
    connection-state=established,related
add action=drop chain=input comment="defconf: drop all from WAN" \
    in-interface=eth1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" \
    connection-state=established,related
add chain=forward comment="defconf: accept established,related" \
    connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" \
    connection-state=invalid
add action=drop chain=forward comment=\
    "defconf:  drop all from WAN not DSTNATed" connection-nat-state=!dstnat \
    connection-state=new in-interface=eth1
/system identity
set name=mkt11
/system routerboard settings
set cpu-frequency=650MHz protected-routerboot=disabled
/tool mac-server
set [ find default=yes ] disabled=yes
add
/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes
add
```

### Crear interfaces y bridges

Primero hay que pensar en capa 2, vamos a crear un bridge por cada red wifi
que tenga un punto de red cableado asociado a una vlan

Trabajaremos con dos vlans:

*111: red pública
*211: red privada

Hay que crear unas interfaces virtuales wifi adicional y cambiar los ssids
para reconocerlas cuando escaneemos las wifis

```
/interface wireless set 0 name=wFree ssid="free111"
/interface wireless add ssid="private211" name=wPrivate master-interface=wFree
```

###[ejercicio2] Explica por qué aparecen las interfaces wifi como disabled
al hacer /interface print. Habilita y deshabilita estar interfaces. Cambiales
el nombre a wFree wPrivate y dejalas deshabilitadas

Haciendo interface wireless enabled/disabled activamos o deshabilitamos la wifi

/interface wireless enabled
/interface wireless disabled

[admin@mkt11] > interface print 
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS 
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:C3:89:8F
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:C3:89:90
 2     eth3                                ether            1500  1598       2028 6C:3B:6B:C3:89:91
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:C3:89:92
 4  X  wFree                               wlan             1500  1600       2290 6C:3B:6B:C3:89:93
 5  X  wPrivate                            wlan                   1600       2290 6E:3B:6B:C3:89:93

[admin@mkt11] > interface wireless set wifi1 name=wFree 
[admin@mkt11] > interface wireless set wlan2 name=wPrivate 


 Para deshabilitar una interficie lo haremos mediante la letra X

## Crear interfaces virtuales y hacer bridges

```
/interface vlan add name eth4-vlan111 vlan-id=111 interface=eth4
/interface vlan add name eth4-vlan211 vlan-id=211 interface=eth4   

/interface bridge add name=br-vlan111
/interface bridge add name=br-vlan211


<<<<<<< HEAD
/interface bridge port add interface=eth4-vlan111 bridge=br-vlan111
/interface bridge port add interface=eth3 bridge=br-vlan111
/interface bridge port add interface=wFree  bridge=br-vlan111     
=======
``` 
>>>>>>> upstream/master


/interface bridge port add interface=eth4-vlan211 bridge=br-vlan211
/interface bridge port add interface=wPrivate   bridge=br-vlan211
/interface print
```

###[ejercicio3] Comenta cada una de estas líneas de la configuración6 
anterior para que quede bien documentado

[admin@mkt11] > /interface print 
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:C3:89:8F
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:C3:89:90
 2     eth3                                ether            1500  1598       2028 6C:3B:6B:C3:89:91
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:C3:89:92
 4  X  wFree                               wlan             1500  1600       2290 6C:3B:6B:C3:89:93
 5  X  wPrivate                            wlan                   1600       2290 6E:3B:6B:C3:89:93
 6     eth4-vlan111                        vlan             1500  1594            6C:3B:6B:C3:89:92
 7     eth4-vlan211                        vlan             1500  1594            6C:3B:6B:C3:89:92

Las dos primera comandas añadimos dos vlans nombradas eth4 - 111 y 211, luego
añadimos dos bridges con estos nombres y vemos que nos aparace todo correcto,
la tercera comanda añadimos tres puertos en la vlan111 y la wififree y le  indicaremos por el bridge donde 
tiene que salir el puerto y finalmente haremos lo mismo pero con la vlan211 i la wifi private


[admin@mkt11] > /interface print 
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:C3:89:8F
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:C3:89:90
 2   S eth3                                ether            1500  1598       2028 6C:3B:6B:C3:89:91
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:C3:89:92
 4  XS wFree                               wlan             1500  1600       2290 6C:3B:6B:C3:89:93
 5  X  wPrivate                            wlan                   1600       2290 6E:3B:6B:C3:89:93
 6  R  br-vlan111                          bridge           1500  1594            6C:3B:6B:C3:89:92
 7  R  br-vlan211                          bridge           1500  1594            6C:3B:6B:C3:89:92
 8   S eth4-vlan111                        vlan             1500  1594            6C:3B:6B:C3:89:92
 9   S eth4-vlan211                        vlan             1500  1594            6C:3B:6B:C3:89:92



### [ejercicio4] Pon una ip 172.17.111.1/24 a br-vlan111 
y 172.17.211.1/24 a br-vlan211 y verifica desde la interface de la placa 
base de tu ordenador que hay conectividad (puedes hacer ping) configurando
unas ips cableadas. Cuando lo hayas conseguido haz un backup de la configuración


ip link add  link enp2s0 name v211 type vlan id 211
ip link add  link enp2s0 name v111 type vlan id 111


[admin@mkt11] > /ip address print 
Flags: X - disabled, I - invalid, D - dynamic 
 #   ADDRESS            NETWORK         INTERFACE                                                                 
 0   ;;; defconf
     192.168.88.1/24    192.168.88.0    eth2                                                                      
 1   172.17.111.1/24    172.17.111.0    br-vlan111                                                                
 2   172.17.211.1/24    172.17.211.0    br-vlan211                                                                

[root@j11 ~]# ping 172.17.211.1
PING 172.17.211.1 (172.17.211.1) 56(84) bytes of data.
From 172.17.211.90 icmp_seq=1 Destination Host Unreachable
From 172.17.211.90 icmp_seq=2 Destination Host Unreachable
From 172.17.211.90 icmp_seq=3 Destination Host Unreachable
From 172.17.211.90 icmp_seq=4 Destination Host Unreachable
^C
--- 172.17.211.1 ping statistics ---
6 packets transmitted, 0 received, +4 errors, 100% packet loss, time 5000ms
pipe 4
[root@j11 ~]# ping 172.17.111.1
PING 172.17.111.1 (172.17.111.1) 56(84) bytes of data.
64 bytes from 172.17.111.1: icmp_seq=1 ttl=64 time=0.396 ms
64 bytes from 172.17.111.1: icmp_seq=2 ttl=64 time=0.274 ms
64 bytes from 172.17.111.1: icmp_seq=3 ttl=64 time=0.250 ms
64 bytes from 172.17.111.1: icmp_seq=4 ttl=64 time=0.279 ms
64 bytes from 172.17.111.1: icmp_seq=5 ttl=64 time=0.262 ms
64 bytes from 172.17.111.1: icmp_seq=6 ttl=64 time=0.215 ms


Vemos que no funciona el ping, cambiamos el puerto eth4 y creamos dos vlans en 
nuestra placa y vemos que funciona correctamente:


6: v111@enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 14:da:e9:99:a1:bc brd ff:ff:ff:ff:ff:ff
    inet 172.17.111.90/24 scope global v111
       valid_lft forever preferred_lft forever
    inet6 fe80::16da:e9ff:fe99:a1bc/64 scope link 
       valid_lft forever preferred_lft forever
7: v211@enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 14:da:e9:99:a1:bc brd ff:ff:ff:ff:ff:ff
    inet 172.17.211.90/24 scope global v211
       valid_lft forever preferred_lft forever
    inet6 fe80::16da:e9ff:fe99:a1bc/64 scope link 
       valid_lft forever preferred_lft forever
[root@j11 ~]# 


[root@j11 ~]# ping 172.17.111.1
PING 172.17.111.1 (172.17.111.1) 56(84) bytes of data.
64 bytes from 172.17.111.1: icmp_seq=1 ttl=64 time=0.426 ms
64 bytes from 172.17.111.1: icmp_seq=2 ttl=64 time=0.277 ms
^C
--- 172.17.111.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1000ms
rtt min/avg/max/mdev = 0.277/0.351/0.426/0.076 ms
[root@j11 ~]# ping 172.17.211.1
PING 172.17.211.1 (172.17.211.1) 56(84) bytes of data.
64 bytes from 172.17.211.1: icmp_seq=1 ttl=64 time=0.466 ms
64 bytes from 172.17.211.1: icmp_seq=2 ttl=64 time=0.293 ms


### [ejercicio5] Crea una servidor dhcp para cada una de las redes en los rangos
.101 a .250 de las respectivas redes. Asignar ip manual a la eth1. 
Crear reglas de nat para salir a internet. 

[admin@mkt11] > ip pool add ranges=172.17.111.101-172.17.111.250 name=range_public
[admin@mkt11] > ip pool add ranges=172.17.211.101-172.17.211.250 name=range_private 

[admin@mkt11] > ip pool add ranges=172.17.111.101-172.17.111.250 name=range_public
[admin@mkt11] > ip pool add ranges=172.17.211.101-172.17.211.250 name=range_private  
[admin@mkt11] > ip address add interface=br-vlan111 address=172.17.111.111 netmask=255.255.255.0
[admin@mkt11] > ip address add interface=br-vlan211 address=172.17.211.211 netmask=255.255.255.0   
[admin@mkt11] > ip address print                                                                
Flags: X - disabled, I - invalid, D - dynamic 
 #   ADDRESS            NETWORK         INTERFACE                                                            
 0   ;;; defconf
     192.168.88.1/24    192.168.88.0    eth2                                                                 
 1   172.17.111.1/24    172.17.111.0    br-vlan111                                                           
 2   172.17.211.1/24    172.17.211.0    br-vlan211                                                           
 3   172.17.111.111/24  172.17.111.0    br-vlan111                                                           
 4   172.17.211.211/24  172.17.211.0    br-vlan211   

[admin@mkt11] > ip dhcp-server add interface=br-vlan111 address-pool=range_public 
[admin@mkt11] > ip dhcp-server add interface=br-vlan211 address-pool=range_private  

[admin@mkt11] > ip dhcp-server network add gateway=172.17.111.1 dns-server=8.8.8.8 netmask=255.255.255.0 address=172.17.111.0/24 domain=davidj11.com
[admin@mkt11] > ip dhcp-server network add gateway=172.17.211.1 dns-server=8.8.8.8 netmask=255.255.255.0 address=172.17.211.0/24 domain=davidj11.com 

Despres fem un dhclient i veiem que ens dona ip automaticament:
dhclient  v211
dchlient  v111

6: v111@enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 14:da:e9:99:a1:bc brd ff:ff:ff:ff:ff:ff
    inet 172.17.111.250/24 brd 172.17.111.255 scope global dynamic v111
       valid_lft 426sec preferred_lft 426sec
7: v211@enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether 14:da:e9:99:a1:bc brd ff:ff:ff:ff:ff:ff

Afegim una ip a la eth1 per comprovar que podem sortir  a internet

[admin@mkt11] /ip> address add address=192.168.3.211 netmask=255.255.0.0 interface=eth1 disabled=no

[admin@mkt11] /ip> address print   
Flags: X - disabled, I - invalid, D - dynamic 
 #   ADDRESS            NETWORK         INTERFACE                                                       
 0   ;;; defconf
     192.168.88.1/24    192.168.88.0    eth2                                                            
 1   172.17.111.1/24    172.17.111.0    br-vlan111                                                      
 2   172.17.211.1/24    172.17.211.0    br-vlan211                                                      
 3   172.17.111.111/24  172.17.111.0    br-vlan111                                                      
 4   172.17.211.211/24  172.17.211.0    br-vlan211                                                      
 5   192.168.3.211/16   192.168.0.0     eth1     
 
                                                        
[admin@mkt11] /ip> route add dst-address=0.0.0.0/0 gateway=192.168.0.1                             

[admin@mkt11] /ip> route print 
Flags: X - disabled, A - active, D - dynamic, 
C - connect, S - static, r - rip, b - bgp, o - ospf, m - mme, 
B - blackhole, U - unreachable, P - prohibit 
 #      DST-ADDRESS        PREF-SRC        GATEWAY            DISTANCE
 0 A S  0.0.0.0/0                          192.168.0.1               1
 1 ADC  172.17.111.0/24    172.17.111.1    br-vlan111                0
 2 ADC  172.17.211.0/24    172.17.211.1    br-vlan211                0
 3 ADC  192.168.0.0/16     192.168.3.211   eth1                      0
 4 ADC  192.168.88.0/24    192.168.88.1    eth2                      0

[admin@mkt11] /ip> /ping 8.8.8.8
  SEQ HOST                                     SIZE TTL TIME  STATUS                 
    0 8.8.8.8                                    56  46 30ms 
    1 8.8.8.8                                    56  46 31ms 
    2 8.8.8.8                                    56  46 30ms 
    3 8.8.8.8                                    56  46 34ms 
    4 8.8.8.8                                    56  46 31ms 
    5 8.8.8.8                                    56  46 30ms 
    6 8.8.8.8                                    56  46 31ms 
    sent=7 received=7 packet-loss=0% min-rtt=30ms avg-rtt=31ms max-rtt=34ms 

[admin@mkt11] > add action=masquerade chain=scrnat out-interface=eth1

[admin@mkt11] > ip firewall nat export                                                
# mar/28/2017 13:29:43 by RouterOS 6.33.5
# software id = KHNI-GJGJ
#
/ip firewall nat
add action=masquerade chain=scrnat out-interface=eth1
[admin@mkt11] > ip firewall nat print                               
Flags: X - disabled, I - invalid, D - dynamic 
 0    chain=srcnat action=masquerade out-interface=eth1 log=no log-prefix=""
 
Amb la ultima comanda, posem un masquerade per poder sortir fora a traves de 
la placa base, fem dhclient a una de las vlans creadas i veiem que fen ping tenim sortida
a internet.

[root@j11 ~]# ping www.google.es
PING www.google.es (216.58.210.163) 56(84) bytes of data.
64 bytes from mad06s10-in-f3.1e100.net (216.58.210.163): icmp_seq=1 ttl=53 time=11.8 ms
64 bytes from mad06s10-in-f3.1e100.net (216.58.210.163): icmp_seq=2 ttl=53 time=11.6 ms
64 bytes from mad06s10-in-f3.1e100.net (216.58.210.163): icmp_seq=3 ttl=53 time=10.7 ms
64 bytes from mad06s10-in-f3.1e100.net (216.58.210.163): icmp_seq=4 ttl=53 time=11.7 ms


### [ejercicio6] Activar redes wifi y dar seguridad wpa2


### [ejercicio6b] Opcional (monar portal cautivo)

### [ejercicio7] Firewall para evitar comunicaciones entre las redes Free y Private. Limitar
puertos en Free.

### [ejercicio8] Conexión a switch cisco con vlans

### [ejercicio8b] (Opcional) Montar punto de acceso adicional con un AP de otro fabricante

### [ejercicio9] QoS

### [ejercicio9] Balanceo de salida a internet

### [ejercicio10] Red de servidores con vlan de servidores y redirección de puertos

### [ejercicio11] Firewall avanzado

